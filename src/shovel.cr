require "uri"
require "file_utils"
require "http"
require "xml"
require "json"
require "option_parser"

require "./misc.cr"
require "./download.cr"
require "./service.cr"
require "./services/*"

debug = false

OptionParser.parse ARGV.empty? ? Array.new(1, "-h") : ARGV do |p|
  p.banner = "dig up all the media."

  p.on("-h", "--help", "show this help") { abort p }
  p.on("-d", "--debug", "print src and dst, then exit") { debug = true }
  p.on("--unsafe", "set stricter formatting") { Service::Info.safe = false }
  p.on("-u <url>", "--url <url>", "specify media url") { |url|
    Service.services.find(&.valid_target? url)
      .try &.instance.download url do |url, info|

      if debug
        puts url.to_s + "\n" + info.format
        break
      end

      download url, info.format
    end
  }
  p.on("-p <path>", "--path <path>", "specify a path to download to") { |p|
    Service::Info.path = p }
  p.on("-f <formatter>", "--format <formatter>", "specify filename formatter") { |f|
    Service::Info.formatter = f }
end
