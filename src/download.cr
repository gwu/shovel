def download(urls : Hash(String, String), dst : String)
  FileUtils.mkdir_p("/tmp/shovel")
  urls.each do |k, url|
    download url, "/tmp/shovel/#{k}"
  end

  FileUtils.mkdir_p(File.dirname dst)
  `ffmpeg #{urls.keys.unshift("").join(" -i /tmp/shovel/")} -c copy #{dst}`
  FileUtils.rm urls.keys.map { |dst| "/tmp/shovel/#{dst}" }
end

def download(url : String, dst : String)
  unless File.exists? dst
    HTTP::Client.get(url) do |r|
      FileUtils.mkdir_p(File.dirname dst)
      File.open(dst, "w") do |file|
        if r.headers["Content-Length"]?
          copy(r.body_io, file) do |p|
            progress = p * 100 / r.headers["Content-Length"].to_i
			progress = progress > 0 ? progress : 0
            size = Terminal.get_size.ws_col

            left = "["
            right = "] #{progress.to_s}%"

            size -= right.size + left.size
			bar_length = size * progress / 100

            bar = ("#" * bar_length) + (" " * (size - bar_length))

            print "\r"
            print left + bar + right
          end
        # else
        end
        puts
      end
    end
  else
    puts "already downloaded: " + dst
  end
end

def retry(&block)
  loop do
    begin
      return yield
    rescue
    end
  end
end
