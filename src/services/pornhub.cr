class Pornhub < Service::Base
  HOST = "https://www.pornhub.com"

  def get(url, &block)
    return unless body = HTTP::Client.get(url).body
    return unless data = JSON.parse body.match(/var flashvars_[0-9]+ = (.*);\n/).not_nil![1]

    best = data["mediaDefinitions"].as_a
      .select { |o| !o["quality"].as_a? && !o["videoUrl"].to_s.empty? }
      .sort_by(&.["quality"].to_s.to_i)
      .last

    yield best["videoUrl"].to_s,
      Service::Info.new(
        {
          "title" => data["video_title"].to_s,
          "ext"   => "mp4",
        },
        "%[title].%[ext]",
      )
  end

  def download(url, &block)
    get(url) { |url, info| yield url, info }
  end
end
