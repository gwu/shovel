class Bandcamp < Service::Base
  HOST = "bandcamp.com"

  private def get_album(url, &block)
    return unless body = HTTP::Client.get(URI.parse url).body
    return unless artist = body.match(/artist: "(.*?)",\n/).try &.[1]
    return unless album = body.match(/"title":"(.*?)",/).try &.[1]
    return unless tracks = JSON.parse(body.match(/trackinfo: (.*?),\n/).try(&.[1]).catch { return }).as_a

    tracks.each_with_index do |o, i|
      info = {
        "title"  => o["title"].to_s,
        "artist" => artist,
        "ext"    => "mp3",
      }

      formatter = "%[artist]/#{
        tracks.size == 1 ? "%[artist]" : "%[album]/%[artist] - %[album] - %[index]"
      } - .%[title].%[ext]"

      if tracks.size != 1
        info.merge!({
            "album" => album,
            "index" => sprintf("%0#{tracks.size.to_s.size}i", i),
        })
      end

      yield o["file"]["mp3-128"].to_s, Service::Info.new(info, formatter)
    end
  end

  private def get_artist(username, &block)
    uri = URI.parse "https://#{username}.#{HOST}"
    XML.parse_html(HTTP::Client.get(uri).body)
	  .xpath_nodes("//li[@class]")
	  .select { |o| o["class"].includes? "music-grid-item" }
	  .map { |o| o.xpath_node("a").not_nil!["href"] }
      .each { |path|
		get_album "#{uri.to_s}#{path}" { |url, info| yield url, info }
      }
  end

  def download(url, &block)
    uri = URI.parse url
    case
    when uri.path.try &.strip("/").split("/")[0] == "album"
      get_album(url) { |url, info| yield url, info }
    else
      return unless username = uri.host.try &.split(".")[0]
      get_artist(username) { |url, info| yield url, info }
    end
  end
end
