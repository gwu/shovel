class Ehentai < Service::Base
  HOST = "https://e-hentai.org"

  private def get_gallery(uri, &block)
    downloaded_pages = 0
    i = 0
    current_page = 0

    loop do
      uri.query = HTTP::Params.encode({"p" => i.to_s})
      return unless body = HTTP::Client.get(uri).body
      return unless root = XML.parse_html(body)

      return unless gallery_size = root
                      .xpath_nodes("//td[@class='gdt2']")
                      .find(&.content.includes? "pages")
                      .try &.content.match(/[0-9]+/)
                        .try &.[0].to_i

      return unless title = root.xpath_node("//h1[@id='gn']").try &.content

      return unless pages = root
                      .xpath_nodes("//div[@class='gdtm']/div/a")
                      .map &.["href"]

      pages.each do |url|
        next unless url = XML.parse_html(HTTP::Client.get(url).body)
                      .xpath_node("//img[@id='img']").try &.["src"]

        yield url,
          Service::Info.new(
            {
              "title"    => title,
              "ext"      => (ext = url.split(".").last),
              "filename" => url.split("/").last.split(".").reverse.shift(1).reverse.join,
              "pages"    => gallery_size.to_s,
              "index"    => sprintf("%0#{gallery_size.to_s.size}i", current_page),
            },
            "%[title]/%[index].%[ext]"
          )

        current_page += 1
      end

      i += 1
      downloaded_pages += pages.size
      break if downloaded_pages >= gallery_size
    end
  end

  def download(url)
    return unless uri = URI.parse url
    return unless path = uri.path.try &.strip("/").split("/")
    if path[0] == "g" && path.size == 3
      get_gallery(uri) { |url, info| yield url, info }
    end
  end
end
