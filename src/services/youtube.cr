require "csv"
class YouTube < Service::Base
  HOST       = "https://www.youtube.com"
  HOST_SHORT = "https://youtu.be"

  def download(url)
    uri = URI.parse(url)
    type = uri.path.try &.strip("/").split("/")[0]

    case type
    when "channel", "user"
      return unless user_id = uri.path.try &.strip("/").split("/")[1]
      get_user(user_id, type) { |url, info| yield url, info }
    when "watch"
      return unless query = uri.query
      return unless id = HTTP::Params.parse(query)["v"]
      get_video(id) { |url, info| yield url, info }
    end
  end

  private def get_video(id, &block)
    uri       = URI.parse HOST
    uri.path  = "/watch"
    uri.query = HTTP::Params.encode({"v" => id})

    return unless root = XML.parse_html HTTP::Client.get(uri).body
    return unless config = root
                    .content
                    .match(/ytplayer\.config = (.*?);ytplayer\.load = function()/)
                    .try &.[1]

    JSON.parse(config)["args"]["adaptive_fmts"].to_s.split(",").each do |x|
      HTTP::Params.parse(x).each do |k,v|
        puts "#{k}: #{v}"
      end
    end

    # urls = {} of String => String
    # player_response["streamingData"]["adaptive_fmts"]
    #   .as_a.group_by(&.["mimeType"].to_s.split("/").first)
    #   .map do |k, o|
    #     urls.merge!({k => o.sort_by(&.["contentLength"].to_s.to_i64).last["url"].to_s})
    #   end

    # yield urls,
    #   Service::Info.new(
    #     {
    #       "title"   => player_response["videoDetails"]["title"].to_s,
    #       "channel" => player_response["videoDetails"]["author"].to_s,
    #       "ext"     => "mkv",
    #     },
    #     "%[channel]/%[title].%[ext]",
    #   )
  end

  private def get_user(user_id, type, &block)
    (uri = URI.parse HOST).path = "/#{type}/#{user_id}/videos"

    return unless root = XML.parse_html HTTP::Client.get(
                    uri,
                    headers: HTTP::Headers{
                      "User-Agent" => "Opera/7.50 (Windows XP; U)",
                    }
                  ).body

    root.xpath_nodes("//h3[@class='yt-lockup-title ']/a")
      .map(&.["href"].split("=").last)
      .each do |id|
        get_video(id) { |url, info| yield url, info }
      end
  end
end
