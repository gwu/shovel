class Fourchan < Service::Base
  def self.valid_target?(url)
    url.includes?("4chan.org/") || url.includes?("4channel.org/")
  end

  def parse_node(node)
    img_url = node.xpath_node("a[@class='fileThumb']").not_nil!.["href"]
    img_url = "https:" + img_url if img_url.starts_with? "//"

    title_node = node.xpath_node("div[@class='fileText']/a").not_nil!

    {
      img_url,
      Service::Info.new(
        {
          "original_name" => title_node["title"]? || title_node.content,
          "ext"           => img_url.split('.').last,
          "id"            => img_url.split('/').last.split('.').first,
        },
        "%[original_name]"
      ),
    }
  end

  private def get_raw(url)
    XML.parse_html(HTTP::Client.get(url).body)
      .try &.xpath_nodes("//div[@class='file']").map { |node| parse_node node }
  end

  def download(url, &block)
    get_raw(url).try &.each { |url, info| yield url, info }
  end
end
