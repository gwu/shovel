class Soundgasm < Service::Base
  HOST = "soundgasm.net"

  struct Track
    getter title, url

    def initialize(@title : String, @url : String)
    end
  end

  private def parse_page(response)
    data = XML.parse_html(response)
    return unless title = data
	  .xpath_node("//div[@class='jp-title']")
      .try &.content

    return unless url = data.to_s
	  .match(/m4a: "(.*?\.m4a)"/)
      .try &.[1]

    Track.new(title, url)
  end

  private def get(url)
    uri = URI.parse(url)
    author = uri.path.not_nil!.strip("/").split("/")[1]
    response = HTTP::Client.get(uri).body
    abort "no response from #{uri.to_s}" unless response

    return unless page_info = parse_page response
    abort "couldn't parse page" unless url

    yield page_info.url,
      Service::Info.new({
          "artist" => author,
          "title"  => page_info.title[0..100],
          "ext"    => uri.path.not_nil!.split(".").last,
        },
        "%[artist]/%[artist] - %[title].%[ext]"
      )
  end

  private def get_user(username)
    (uri = URI.parse "https://" + HOST).path = File.join({"/u", username})
    XML.parse_html(HTTP::Client.get(uri).body)
      .xpath_nodes("//div[@class='sound-details']/a")
      .each do |url|
        get(url["href"]) { |url, info| yield url, info }
      end
  end

  def download(url, &block)
    path = URI.parse(url).path.not_nil!.strip("/").split("/")
    case path.size
    when 2
      get_user(path[1]) { |url, info| yield url, info }
    when 3
      get(url) { |url, info| yield url, info }
    end
  end
end
