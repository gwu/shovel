class SoundCloud < Service::Base
  HOST     = "soundcloud.com"
  ENDPOINT = "https://api.soundcloud.com"

  # thanks scdl
  CLIENT_ID = "a3e059563d7fd3372b49b37f00a00bcf"
  # CLIENT_ID = "2t9loNQH90kzJcsFCODdigxfp325aq4z"

  private def get_stream(url)
    (uri = URI.parse url)
      .query = HTTP::Params.encode({"client_id" => CLIENT_ID})
    rsp = HTTP::Client.get(uri)
    return JSON.parse(rsp.body)["location"].to_s if rsp.status_code == 302
  end

  private def get_track(url)
    uri       = URI.parse ENDPOINT
    uri.path  = "/resolve"
    uri.query = HTTP::Params.encode({"client_id" => CLIENT_ID, "url" => url,})

    track_json =
      JSON.parse HTTP::Client.get(
        JSON.parse(HTTP::Client.get(uri).body)["location"].to_s).body

    unless location = get_stream track_json["download_url"].to_s
      return unless location = get_stream track_json["stream_url"].to_s
    end
    puts location


    yield location,
      Service::Info.new({
          "artist" => track_json["user"]["username"].to_s,
          "title"  => track_json["title"].to_s,
          "ext"    => URI.parse(location).path.not_nil!.split(".").last,
        },"%[artist]/%[artist] - %[title].%[ext]")
  end

  def download(url)
    get_track url do |url, info|
      yield url, info
    end
  end
end
