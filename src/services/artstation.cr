class ArtStation < Service::Base
  HOST = "https://www.artstation.com"

  def get_user_projects(username)
    (uri = URI.parse HOST).path = "/users/#{username}/projects.json"

    i = 0
    loop do
      uri.query = HTTP::Params.encode({"page" => (i += 1).to_s})
      return unless body = HTTP::Client.get(uri).body
      return unless projects = JSON.parse body

      projects["body"].as_a.each do |project|
        get_assets project["hash_id"].to_s do |url, info|
          yield url, info
        end
      end
    end
  end

  def get_assets(hash_id)
    (uri = URI.parse HOST).path = "/projects/#{hash_id}.json"
    return unless body = retry { HTTP::Client.get(uri) }.body
    return unless project = JSON.parse body

    project["assets"].as_a.each_with_index do |asset, i|
      formatter =
        "%[artist]/%[artist] - %[title]#{
          project["assets"].as_a.size == 1 ? "" : "-%[index]"
        }.%[format]"

      yield asset["image_url"].to_s,
        Service::Info.new(
          {
            "title"  => project["title"].to_s,
            "artist" => project["user"]["full_name"].to_s,
            "format" => URI.parse(asset["image_url"].to_s).path.not_nil!.split(".").last,
            "index"  => sprintf("%0#{project["assets"].as_a.size.to_s.size}i", i),
          },
          formatter
        )
    end
  end

  def download(url)
    uri = URI.parse(url)

    get_user_projects uri.path.not_nil!.strip("/").split("/")[0] do |url, info|
      yield url, info
    end
  end
end
