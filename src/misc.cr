class Object
  def catch(&block)
    self
  end
end

struct Nil
  def catch(&block)
    yield
  end
end

def copy(src, dst)
  buffer = uninitialized UInt8[4096]
  count = 0
  while (len = src.read(buffer.to_slice).to_i32) > 0
    dst.write buffer.to_slice[0, len]
    count += len
    yield count
  end
  len < 0 ? len : count
end

module Terminal
  extend self

  lib C
    struct Winsize
      ws_row : UInt16    # rows, in characters
      ws_col : UInt16    # columns, in characters
      ws_xpixel : UInt16 # horizontal size, pixels
      ws_ypixel : UInt16 # vertical size, pixels
    end

    TIOCGWINSZ = 0x5413  # TERMINAL WINDOW SIZE

    fun ioctl(fd : Int32, request : UInt32, winsize : C::Winsize* ) : Int32
  end

  def get_size
    C.ioctl(0, C::TIOCGWINSZ, out screen_size)
    return screen_size
  end
end
