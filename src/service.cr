module Service
  class_property services = [] of Service::Base.class

  abstract class Base
    macro inherited
      Service.services << self

      def self.instance
        @@instance ||= new
      end

      def self.valid_target?(url)
        url.includes? HOST
      end
    end
  end

  struct Info
    class_property safe = true, path = Dir.current, formatter = ""
    property formatter, info

    def initialize(@info : Hash(String, String), @formatter : String)
    end

    def format
      formatter, info, safe = @formatter, @info, @@safe

      formatter.scan(/%\[(\w+)\]/).map(&.[1]).each do |match|
          break unless match
          formatter = formatter
            .gsub "%[#{match}]", (info[match]? ? info[match] : "unknown").to_s
      end

      formatter = formatter.gsub(safe ? /[^A-Za-z0-9-_.]/ : /[\/]/, "_") if safe

	  if (formatter.size > 250)
		formatter = formatter[0, 250]
	  end

      return File.join({ @@path, formatter })
    end
  end
end
