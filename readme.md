# shovel

shovel digs up all the media for you.

works for:
- 4chan
- artstation
- bandcamp
- imgur
- nhentai
- pornhub
- soundcloud
- soundgasm
- youtube

## Installation

- [Source](#source)
- [Bin](#bin)

### Source

```
git clone gitlab.com/gwu/shovel.git
cd shovel
mkdir bin
crystal build 'src/shovel.cr' -o 'bin/shovel'
```

### Bin

go to CI/CD on your gitlab sidebar
and on the right will be a "download artifacts" button
unzip this file and put it anywhere in your `$PATH`

## Usage

```
 ~ > shovel -h
me download everything uwu
    -h, --help                       show this help
    -d, --debug                      only
    -u <url>, --url <url>            specify media url
    -p <path>, --path <path>         specify a path to download to
    -f <formatter>, --format <formatter>
                                     specify filename formatter
```

## Contributing

1. Fork it (<https://github.com/your-github-user/shovel/fork>)
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request

## Contributors

- [gwu](https://gitlab.com/gwu) - creator and maintainer
- [mlvzk](https://gitlab.com/mlvzk) - crystal enthusiast
